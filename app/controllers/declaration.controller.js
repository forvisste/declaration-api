let declarations = {
	declaration1: {
		id: 1,
		firstname: "Ibtissam",
		lastname: "OUARCHAN",
		number: 0654306733,
		adress: "Rue Med5 Quartier Bulveder",
		description: "Bonjour, je veux déclarer d'une cas dans notre ville "
	},
	declaration2: {
		id: 2,
		firstname: "Younes",
		lastname: "CHABRAOUI",
		number: 0647561575,
		adress: "hay  farah",
		description: "Bonjour, je veux déclarer d'une cas dans notre ville "
	},
	declaration3: {
		id: 3,
		firstname: "jamila",
		lastname: "Arbi",
		number: 0464546373,
		adress: "hay nassim",
		description: "Bonjour, je veux déclarer d'une cas dans notre ville "
	},
	declaration4: {
		id: 4,
		firstname: "zakaria",
		lastname: "Labyad",
		number: 0958748483,
		adress: "hay tadart",
		description: "Bonjour, je veux déclarer d'une cas dans notre ville "
	},
	declaration5: {
		id: 5,
		firstname: "salma",
		lastname: "naciri",
		number: 0895579846,
		adress: "hay mly abdellah",
		description: "Bonjour, je veux déclarer d'une cas dans notre ville "
	}
	
}

exports.create = function(req, res) {
	// find the largest ID
	let arr = Object.keys( declarations ).map(function ( key ) { return declarations[key].id; });
	let newId = Math.max.apply( null, arr ) + 1;
	
	let newDeclaration = req.body;
	newDeclaration.id = newId;
    declarations["declaration" + newId] = newDeclaration;
    res.json(newDeclaration);
};
 
exports.findAll = function(req, res) {
    res.json(Object.values(declarations));  
};
 
exports.findOne = function(req, res) {
    let declaration = declarations["declaration" + req.params.id];
    res.json(declaration);
};
 
exports.update = function(req, res) {
	let updatedDeclaration = req.body; 
	declarations["declaration" + updatedDeclaration.id] = updatedDeclaration;
	res.json({msg: "Member Updated Successfully!"});
};
 
exports.delete = function(req, res) {
    delete declarations["declaration" + req.params.id];
    res.json({msg: "Member Deleted Successfully!"});
};