let express = require('express');
let router = express.Router();

let declarations = require('../controllers/declaration.controller.js');

// Create a new Customer
router.post('/api/declarations', declarations.create);

// Retrieve all Customer
router.get('/api/declarations', declarations.findAll);

// Retrieve a single Customer by Id
router.get('/api/declarations/:id', declarations.findOne);

// Update a Customer with Id
router.put('/api/declarations', declarations.update);

// Delete a Customer with Id
router.delete('/api/declarations/:id', declarations.delete);

module.exports = router;